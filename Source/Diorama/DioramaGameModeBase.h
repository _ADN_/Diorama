// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DioramaGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DIORAMA_API ADioramaGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
