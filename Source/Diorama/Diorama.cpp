// Copyright Epic Games, Inc. All Rights Reserved.

#include "Diorama.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Diorama, "Diorama" );
